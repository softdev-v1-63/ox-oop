/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gog.mainprogram;
import java.util.Scanner;

/**
 *
 * @author HP
 */
public class Game {
    Player playerX;
    Player playerO;
    Player turn;
    Table table;
    int row,col;
    Scanner as = new Scanner(System.in);
    public Game(){
        playerX = new Player('X');
        playerO = new Player('O');
        table = new Table(playerX,playerO);
    }
    
    public void showWelcom(){
        System.out.println("Welcome to OX Game");
    }
    public void showTable(){
        table.showTable();
    }
    public void input(){
        while (true) {            
            System.out.println("Please input Row Col : ");
            row = as.nextInt()-1;
            col = as.nextInt()-1;
            if(table.setRowCol(row,col)){
                break;
            }
            System.out.println("Error: table at row and col is not empty!!");   
        }
    }
    public void showTurn(){
        System.out.println(table.getCurrentPlayer().getName() + " turn");
    }

    public Game(Player playerX, Player playerO, Player turn, Table table, int row, int col) {
        this.playerX = playerX;
        this.playerO = playerO;
        this.turn = turn;
        this.table = table;
        this.row = row;
        this.col = col;
    }
    
    public void newGame(){
        table = new Table(playerX,playerO);
    }
    
    public void run(){
        this.showWelcom();
        while (true) {            
            this.showTable();
            this.showTurn();
            this.input();
            table.checkWin();
            if(table.isFinish()){
                if(table.getWinner()==null){
                    System.out.println("Draw!!");
                }else{
                    System.out.println(table.getWinner().getName()+" Win!!");
                }
                System.out.println("You want to play ? Y/N");
                char replay = as.next().charAt(0);
                if(replay == 'Y'){
                    newGame();
                }else {
                    System.out.println("Thank for play !!");
                    break;
                }
            }
            table.switchPlayer();
        }    
    }
}
