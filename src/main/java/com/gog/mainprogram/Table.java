package com.gog.mainprogram;

import javax.print.attribute.standard.Finishings;

public class Table {
    private char[][] table = {
        {'-','-','-'},
        {'-','-','-'},
        {'-','-','-'}
    };
    private Player playerX;
    private Player playerO;
    private Player currentPlayer;
    private int lastCol,lastRow;
    private Player winner;
    private boolean isFinish;   
    
    public Table(Player x,Player o){
        playerX = x;
        playerO = o;
        currentPlayer = x;
    }
    public void showTable(){
        System.out.println("  1 2 3");
        for(int i=0;i<3;i++){
            System.out.print(i+" ");
            for(int j=0;j<3;j++){
                System.out.print(table[i][j]+" ");
            }
            System.out.println("");
        }
    }
    public boolean setRowCol(int row,int col){
        if(table[row][col] == '-'){
            table[row][col] = currentPlayer.getName();
            this.lastRow = row;
            this.lastCol = col;
            return true;
        }
        return false;
    }
    public Player getCurrentPlayer(){
        return currentPlayer; 
    }
    public void switchPlayer(){
        if(currentPlayer==playerX){
            currentPlayer = playerO;
        }else{
            currentPlayer = playerX;
        }
    }
    void checkCol(){
        for(int row=0;row<3;row++){
            if(table[row][lastCol] != currentPlayer.getName()){
                return;
            }
        }
        isFinish =true;
        winner = currentPlayer;
        setStatWinLose();
    }

    private void setStatWinLose() {
        if(currentPlayer==playerO){
            playerO.win();
            playerX.lose();
        }else{
            playerX.win();
            playerO.lose();
        }
    }
    
    void checkRow(){
        for(int col=0;col<3;col++){
            if(table[lastRow][col] != currentPlayer.getName()){
                return;
            }
        }
        isFinish =true;
        winner = currentPlayer;
        setStatWinLose();
        
    }
    
    void checkXL(){
        for(int i=3;i>0;i--){
           if(table[lastRow][i-1]!=currentPlayer.getName()){
                return;
           }
       }
        isFinish =true;
        winner = currentPlayer;
        setStatWinLose();
    }
    void checkXR(){
       for(int i=0;i<3;i++){
           if(table[i][i]!=currentPlayer.getName()){
                return;
           }
       }
        isFinish =true;
        winner = currentPlayer;
        setStatWinLose();
    }
    public void checkWin(){
        checkCol();
        checkRow();
        checkXR();
        checkXL();
    }
    
    public boolean isFinish(){
        return isFinish;
    }
    public Player getWinner(){
        return winner;
    }

    

    
}
